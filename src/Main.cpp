#include <iostream>
#include <stdexcept>

#include <SDL.h>
#include <SDL_net.h>

#include "Configuration.h"
#include "Server.h"

class InitializationError : public std::runtime_error
{
public:
	InitializationError(const char* what)
		: std::runtime_error(what)
	{
	}
};

int main(int argc, char** argv)
{
	try
	{
		if (SDL_Init(SDL_INIT_EVENTS | SDL_INIT_TIMER) != 0)
		{
			throw InitializationError(SDL_GetError());
		}
		if (SDLNet_Init() != 0)
		{
			throw InitializationError(SDLNet_GetError());
		}

		ServerConfiguration conf;
		conf.SetPort(7777);

		ServerConfiguration::SetCurrent(conf);

		Server server(conf);

		bool running = true;
		server.Start();
		while (running)
		{
			running = server.Update();
		}
	}
	catch (const std::exception& exception)
	{
		std::cerr << "\n\n**************************\n*\n";
		std::cerr << "*   ERROR: " << exception.what() << std::endl;
		std::cerr << "*\n**************************\n";
		std::cin.get();
		return -1;
	}
	return 0;
}