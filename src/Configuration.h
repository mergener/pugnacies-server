#pragma once

#include <string>
#include <istream>
#include <fstream>

#include <SDL_net.h>

#define DEFAULT_CONFIGURATION_FILE_NAME "svconfig.config"

class ServerConfiguration
{
public:
	static ServerConfiguration& GetCurrent() { return *current; }
	static void SetCurrent(ServerConfiguration& conf) { current = &conf; }
	
	ServerConfiguration(const std::string& filepath);
	ServerConfiguration(std::istream& stream);
	ServerConfiguration();

	inline const std::string& GetStrAddress() const { return strAddress; }
	inline void SetStrAddress(const std::string& s) { strAddress = s; }
	inline Uint16 GetPort() const { return port; }
	inline void SetPort(Uint16 port) { this->port = port; }

private:
	static ServerConfiguration* current;

	std::string strAddress = "localhost";
	Uint16 port = 7777;
};