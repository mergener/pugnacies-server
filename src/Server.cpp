#include "Server.h"

#include <SDL_net.h>

#include <iostream>
#include <stdexcept>

#include <NetworkMessages.h>

#include "ServerEvents.h"
#include "Database.h"

#define DEFAULT_PACKET_SIZE 1500

static UDPsocket OpenSocket(Uint16 port)
{
	UDPsocket socket;
	if (!(socket = SDLNet_UDP_Open(port)))
	{
		throw std::runtime_error(SDLNet_GetError());
	}
	return socket;
}

static UDPpacket* AllocatePacket(int size)
{
	UDPpacket* packet;

	if (!(packet = SDLNet_AllocPacket(size)))
	{
		throw std::runtime_error(SDLNet_GetError());
	}
	
	return packet;
}

Server::Server(const ServerConfiguration& conf)
	:	
	port(conf.GetPort()),
	socket(OpenSocket(port)),
	packet(AllocatePacket(DEFAULT_PACKET_SIZE))
{
}

void Server::Start()
{
	Database::ConnectToExisting("dbname = mydb");
}

bool Server::Update()
{
	if (SDLNet_UDP_Recv(socket, packet))
	{
		NetworkMessage message;
		TranslateNetworkMessage((char*)packet->data, &message);
		HandleNetworkMessage(message);
	}

	return true;
}

void Server::HandleNetworkMessage(const NetworkMessage& message)
{
	switch (message.type)
	{
		case NetworkMessageType::NETMSG_CTS_LOGINREQ:
		{
			char* userName = message.data;
			char* userUnencryptedPassword = message.data;

			while (*userUnencryptedPassword++); // find null character, which indicates the end of the user's name string.

			ServerEvents::OnUserRequestLogin(userName, userUnencryptedPassword);

			break;
		}
	}
}