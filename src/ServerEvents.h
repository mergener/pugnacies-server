#pragma once

namespace ServerEvents
{
	void OnUserRequestLogin(const char* userName, const char* unencryptedUserPassword);
}