#include "User.h"

#include <fstream>
#include <ostream>
#include <istream>
#include <stdexcept>

#include <SDL.h>

UserNotFoundException::UserNotFoundException()
	: std::exception()
{
}

UserRegistrationException::UserRegistrationException(const std::string& msg)
	: std::exception(msg.c_str())
{
}

DatabaseIOException::DatabaseIOException(const std::string& msg)
	: std::exception(msg.c_str())
{
}

static std::unordered_map<std::string, User*> allUsers;

bool User::SetPassword(const UserPassword& oldPassword, const UserPassword& newPassword)
{
	if (oldPassword != password)
	{
		return false;
	}

	this->password = newPassword;
	return true;
}

User::User(const std::string& name, const UserPassword& password)
	: password(password)
{
}

User* User::Register(const std::string& name, const UserPassword& password)
{
	if (allUsers.find(name) == allUsers.end())
	{
		// no username with that name found, it's ok to go
		try
		{
			User* ret = new User(name, password);
			allUsers[name] = ret;
			return ret;
		}
		catch (std::bad_alloc&)
		{
			throw UserRegistrationException("The system ran out of memory when trying to complete the registration.");
		}
	}
	else
	{
		throw UserRegistrationException("An username named " + name + " already existed.");
	}
}

void User::Unregister(User* user)
{
	allUsers.erase(user->GetName());
}

User& User::GetUser(const std::string& name)
{
	try
	{
		return *allUsers.at(name);
	}
	catch (std::exception&)
	{
		throw UserNotFoundException();
	}
}

void User::LoadAllUserData(const std::string& databasePath)
{
	// 1 - Get user count
	// 2 - With the total number of users, start reading each individual user data
	//		2.1 - Read current user name length (uint32)
	//		2.2 - Read current user password length (uint32)
	//		2.3 - Read current user name
	//		2.4 - Read current user password
	//
	//	User data files are little endian, so every step above most be followed by an endian
	//	correction if this is running on a big endian machine.

	try
	{
		std::ifstream stream(databasePath);
		stream.exceptions(std::ios_base::badbit);

		if (SDL_BYTEORDER == SDL_LIL_ENDIAN)
		{
			uint64_t userCount;
			stream.read((char*)&userCount, sizeof(userCount));

			for (int i = 0; i < userCount; ++i)
			{
				uint32_t userNameLength, userPasswordLength;
				
				stream.read((char*)&userNameLength, sizeof(userNameLength));
				stream.read((char*)&userPasswordLength, sizeof(userPasswordLength));

				char* nameBuffer = new char[userNameLength + 1];
				stream.read(nameBuffer, userNameLength);
				nameBuffer[userNameLength] = '\0';

				char* passwordBuffer = new char[userPasswordLength + 1];
				stream.read(passwordBuffer, userPasswordLength);
				passwordBuffer[userPasswordLength] = '\0';

				User* user = new User(nameBuffer, passwordBuffer);

				allUsers[user->GetName()] = user;

				delete nameBuffer;
				delete passwordBuffer;
			}
		}
		else
		{
			uint64_t userCount;
			stream.read((char*)&userCount, sizeof(userCount));
			userCount = SDL_Swap64(userCount);

			for (int i = 0; i < userCount; ++i)
			{
				uint32_t userNameLength, userPasswordLength;

				stream.read((char*)&userNameLength, sizeof(userNameLength));
				stream.read((char*)&userPasswordLength, sizeof(userPasswordLength));

				userNameLength = SDL_Swap32(userNameLength);
				userPasswordLength = SDL_Swap32(userPasswordLength);

				char* nameBuffer = new char[userNameLength + 1];
				stream.read(nameBuffer, userNameLength);
				nameBuffer[userNameLength] = '\0';

				char* passwordBuffer = new char[userPasswordLength + 1];
				stream.read(passwordBuffer, userPasswordLength);
				passwordBuffer[userPasswordLength] = '\0';

				User* user = new User(nameBuffer, passwordBuffer);

				allUsers[user->GetName()] = user;

				delete nameBuffer;
				delete passwordBuffer;
			}
		}
	}
	catch (const std::ofstream::failure& f)
	{
		throw DatabaseIOException("An IO error occurred during the proccess of reading user data from the disk.");
	}
}

void User::SaveAllUserData(const std::string& databasePath)
{
	// 1 - Save user count
	// 2 - Save all users, storing the data using the following pattern for each individual:
	//		2.1 - Save user name length (uint32)
	//		2.2 - Save user password length (uint32)
	//		2.3 - Save user name string
	//		2.4 - Save user password string
	//
	//	User data files are little endian, so every step above most be followed by an endian
	//	correction if this is running on a big endian machine.

	try
	{
		std::ofstream stream(databasePath);
		stream.exceptions(std::ios_base::badbit);

		if (SDL_BYTEORDER == SDL_LIL_ENDIAN)
		{
			uint64_t userCount = GetRegisteredUsersCount();
			stream.write((char*)&userCount, sizeof(userCount));

			for (auto pair : allUsers)
			{
				uint32_t userNameLength = pair.second->GetName().size();
				uint32_t userPasswordLength = pair.second->password.GetEncryptedString().size();

				stream.write((char*)&userNameLength, sizeof(userNameLength));
				stream.write((char*)&userPasswordLength, sizeof(userPasswordLength));

				stream.write(pair.second->GetName().c_str(), userNameLength);
				stream.write(pair.second->password.GetEncryptedString().c_str(), userPasswordLength);
			}
		}
		else
		{
			uint64_t userCount = SDL_Swap64(GetRegisteredUsersCount());
			stream.write((char*)&userCount, sizeof(userCount));

			for (auto pair : allUsers)
			{
				uint32_t userNameLength = SDL_Swap32(pair.second->GetName().size());
				uint32_t userPasswordLength = SDL_Swap32(pair.second->password.GetEncryptedString().size());

				stream.write((char*)&userNameLength, sizeof(userNameLength));
				stream.write((char*)&userPasswordLength, sizeof(userPasswordLength));

				stream.write(pair.second->GetName().c_str(), userNameLength);
				stream.write(pair.second->password.GetEncryptedString().c_str(), userPasswordLength);
			}
		}
	}
	catch (const std::ofstream::failure& f)
	{
		throw DatabaseIOException("An IO error occurred during the proccess of saving user data to the disk.");
	}
}

long User::GetRegisteredUsersCount() noexcept
{
	return allUsers.size();
}

long User::GetRegisteredUsersCount(const std::string& databasePath)
{
	try
	{
		std::ifstream stream(databasePath);
		stream.exceptions(std::ios_base::badbit);

		long ret;
		stream.read((char*)&ret, sizeof(ret));
		
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
		{
			return (long)SDL_Swap64(ret);
		}
		return ret;
	}
	catch (const std::ifstream::failure& f)
	{
		throw DatabaseIOException("An IO error occurred during the proccess of reading user data from the disk.");
	}
}