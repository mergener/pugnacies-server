#include "UserPassword.h"

UserPassword::UserPassword(const std::string& s)
	: password(s) 
{ 
}

UserPassword::UserPassword(const char* s)
	: password(s)
{
}

std::string UserPassword::GetEncryptedString() const
{
	return this->password;
}

bool UserPassword::operator==(const UserPassword& other) const
{
	return this->password == other.password;
}

bool UserPassword::operator!=(const UserPassword& other) const
{
	return this->password != other.password;
}