#pragma once

#include <NetworkMessages.h>

#include "Configuration.h"

class Server
{
public:
	Server(const ServerConfiguration& conf);
	void Start();
	bool Update();

private:
	Uint16 port;
	UDPpacket* packet;
	UDPsocket socket;

	void HandleNetworkMessage(const NetworkMessage& message);
};