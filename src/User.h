#pragma once

#include <string>
#include <unordered_map>
#include <stdexcept>

#include <SDL_net.h>

#include <NetworkMessages.h>

#include "UserPassword.h"

//
//	Exception thrown when trying to search for a username that doesn't match any existing user.
//
class UserNotFoundException : public std::exception
{
public:
	UserNotFoundException();
};

//
//	Exception thrown upon errors during user registration.
//
class UserRegistrationException : public std::exception
{
public:
	UserRegistrationException(const std::string& msg);
};

//
//	Exception thrown upon errors during user database reads or writes.
//
class DatabaseIOException : public std::exception
{
public:
	DatabaseIOException(const std::string& msg);
};

//
//	Defines data and behaviour for every user account registered in the database.
//
class User
{
public:
	inline const std::string& GetName() const { return name; }
	bool IsOnline() const;
	IPaddress GetIpAddress() const;

	//
	//	Sends the specified NetworkMessage to the player if it is online.
	//
	void SendNetworkMessage(const NetworkMessage& msg);

	//
	//	Sets a new password for this user.
	//	Returns true if the password was succesfully changed.
	//
	bool SetPassword(const UserPassword& oldPassword, const UserPassword& newPassword);

	//
	//	Returns true if the entered password matches this user's password.
	//
	bool ValidatePassword(const UserPassword& password) const;

	//
	//	Throws UserRegistrationException if the user was already registered.
	//
	static User* Register(const std::string& name, const UserPassword& password);

	static void Unregister(User* user);

	//
	//	Throws UserNotFoundException if no registered user matches the specified username.
	//
	static User& GetUser(const std::string& username);

	//
	//	Loads all user data stored in the specified database into main memory.
	//
	static void LoadAllUserData(const std::string& databasePath);

	//
	//	Saves all user data stored by main memory in the specified database.
	//
	static void SaveAllUserData(const std::string& databasePath);

	//
	//	Returns the number of all registered users stored by main memory.
	//
	static long GetRegisteredUsersCount() noexcept;
	
	//
	//	Returns the number of all registered users in the specified database.
	//	Throws DatabaseIOException if the specified database doesn't exist or is malformed.
	//
	static long GetRegisteredUsersCount(const std::string& databasePath);

private:

	User(const std::string& name, const UserPassword& password);
	~User();

private:
	//
	//	Data:
	//

	std::string name;
	UserPassword password;

	struct ConnectionStatus
	{
		bool connected;
		IPaddress address;
	};
	ConnectionStatus connectionStatus;
};