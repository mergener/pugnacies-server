#pragma once

#include <string>

class UserPassword
{
public:
	UserPassword(const std::string& s);
	UserPassword(const char* s);
	bool operator==(const UserPassword& other) const;
	bool operator!=(const UserPassword& other) const;
	std::string GetEncryptedString() const;

private:
	std::string password;
};