#include "Configuration.h"

#include <stdexcept>
#include <unordered_map>

class ConfigurationParseError : public std::runtime_error
{
public:
	ConfigurationParseError(const char* what)
		: std::runtime_error(what)
	{
	}
};

using FileInfo = std::unordered_map<std::string, std::string>;

static int FindChar(char* s, size_t size, char c)
{
	for (int i = 0; i < size && s[i] != '\0'; ++i)
	{
		if (s[i] == c)
			return i;
	}
	return -1;
}

static FileInfo* GenerateFileInfo(std::istream& stream)
{
	constexpr size_t bufferSize = 1024;
	char buffer[bufferSize];
	std::string fieldName;
	std::string value;
	FileInfo* ret = new FileInfo();

	while (!stream.eof())
	{
		stream.getline(buffer, bufferSize, ':');
		fieldName = buffer;

		if (!stream.eof())
		{
			stream.getline(buffer, bufferSize);
		}
		else
		{
			throw ConfigurationParseError("An error occurred while reading 'svconfig.config' file, the file is malformed.");
		}

		(*ret)[fieldName] = value;
	}

	return ret;
}

ServerConfiguration* ServerConfiguration::current = nullptr;

ServerConfiguration::ServerConfiguration(const std::string& path)
{
	std::ifstream stream(path);
	ServerConfiguration::ServerConfiguration(stream);
	stream.close();
}

ServerConfiguration::ServerConfiguration(std::istream& stream)
{
	FileInfo* info = GenerateFileInfo(stream);
	SetStrAddress((*info)["addresss"]);	
}

ServerConfiguration::ServerConfiguration()
{
}