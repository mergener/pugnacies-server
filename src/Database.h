#pragma once

#include <string>
#include <stdexcept>

class DatabaseException : public std::runtime_error
{
public:
	DatabaseException(const std::string& msg)
		: std::runtime_error(msg) { }
};

class Database
{
public:
	static Database* Create();
	static Database* ConnectToExisting(const std::string& conninfo);

	void PerformQuery(const std::string& sqlQuery);
	
private:
	Database();
};